export interface SpritesheetSprite {
    name: string;
    center: [number, number];
    size: [number, number];
}

export interface SpritesheetAnimationFrame {
    sprite: string;
    length: number;
}

export interface SpritesheetAnimation {
    name: string;
    frames: SpritesheetAnimationFrame[];
}

export interface Spritesheet {
    source: string;
    sprites: SpritesheetSprite[];
    animations: SpritesheetAnimation[];
}
