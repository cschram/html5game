import * as vscode from "vscode";
import { Rez2DSpritesheetEditorProvider } from "./providers/spritesheet";

export function activate(context: vscode.ExtensionContext) {
    context.subscriptions.push(Rez2DSpritesheetEditorProvider.register(context));
}
