import * as PIXI from "pixi.js";

export interface DrawableContainer {
    container: PIXI.Container;
}
