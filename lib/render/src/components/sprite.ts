import * as PIXI from "pixi.js";
import { createComponent } from "@rez2d/ecs";
import { DrawableContainer } from "./drawable";
import { RenderResources } from "../types";

interface SpriteArgs {
    src: string;
    zIndex?: number;
}

interface SpriteData extends DrawableContainer {
    readonly sprite: PIXI.Sprite;
}

export const Sprite = createComponent<SpriteData, SpriteArgs, RenderResources>((resources, args) => {
    const assets = resources.get("assets").unwrap();
    const renderer = resources.get("renderer").unwrap();
    const texture = assets.get<PIXI.BaseTexture>(args.src)
        .expect(`Unable to load sprite "${args.src}"`);
    const sprite = new PIXI.Sprite(new PIXI.Texture(texture));
    sprite.anchor.set(0.5, 0.5);
    if (args.zIndex) {
        sprite.zIndex = args.zIndex;
    }
    renderer.addChild(sprite);
    return {
        sprite,
        container: sprite,
    };
}, (resources, { sprite }) => {
    const renderer = resources.get("renderer").unwrap();
    renderer.removeChild(sprite);
});
