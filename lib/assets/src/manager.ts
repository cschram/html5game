import { Option, Some, None } from "oxide.ts";
import {
    AssetHandle,
    AssetLoader,
    AssetState,
    AssetManagerConfig,
    AssetLoadFailure,
} from "./types";
export class AssetManager {
    private _loaders: Record<string, AssetLoader<unknown>>;
    private _handles: Record<string, AssetHandle<unknown>>;
    readonly config: AssetManagerConfig;

    constructor(config: AssetManagerConfig) {
        this._loaders = {};
        this._handles = {};
        this.config = config;
    }

    get loading(): number {
        let count = 0;
        for (const handle of Object.values(this._handles)) {
            if (handle.state === AssetState.LOADING) {
                count++;
            }
        }
        return count;
    }

    get failed(): AssetLoadFailure[] {
        return Object.entries(this._handles)
            .reduce<AssetLoadFailure[]>((acc, [filename, handle]) => {
                if (handle.state === AssetState.ERROR) {
                    return acc.concat([
                        {
                            filename,
                            error: handle.error || "Unknown error",
                        },
                    ]);
                }
                return acc;
            }, []);
    }

    registerLoader(extensions: string[], loader: AssetLoader<any>) {
        for (const ext of extensions) {
            if (this._loaders.hasOwnProperty(ext)) {
                throw new Error(`Loader already registered for extension "${ext}"`);
            }
            this._loaders[ext] = loader;
        }
    }

    load(...filenames: string[]): this {
        for (const filename of filenames) {
            if (!this._handles.hasOwnProperty(filename)) {
                const parts = filename.split(".");
                if (parts.length < 2) {
                    throw new Error(`Invalid filename "${filename}"`);
                }
                const ext = parts[parts.length - 1];
                if (!this._loaders.hasOwnProperty(ext)) {
                    throw new Error(`No loader for extension "${ext}"`);
                }
                const path = `${this.config.baseUrl}/${filename}`;
                const handle: AssetHandle<unknown> = {
                    state: AssetState.LOADING,
                    asset: None,
                };
                this._handles[filename] = handle;
                this._loaders[ext](path, this.config)
                    .then((asset) => {
                        handle.state = AssetState.LOADED;
                        handle.asset = Some(asset);
                    })
                    .catch((error) => {
                        handle.state = AssetState.ERROR;
                        handle.error = error;
                    });
            }
        }
        return this;
    }

    get<TAsset>(filename: string): Option<TAsset> {
        if (!this._handles[filename]) {
            return None;
        }
        // There is no real good way to do this in a strictly type-safe way.
        // So if you don't use the right generic arg this is going to break.
        return this._handles[filename].asset as Option<TAsset>;
    }
}
