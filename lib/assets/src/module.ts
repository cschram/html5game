import { Module } from "@rez2d/ecs";
import { AssetManager } from "./manager";
import { AssetManagerConfig } from "./types";

export interface AssetsResources {
    assets: AssetManager;
}

export function AssetsModule<TResources extends AssetsResources>(config: AssetManagerConfig): Module<TResources> {
    return (_store, _schedule, resources) => {
        resources.set("assets", new AssetManager(config));
    };
}
