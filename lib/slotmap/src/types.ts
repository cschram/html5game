import { Option } from "oxide.ts";

export interface SlotKey {
    index: number;
    version: number;
}

export interface Slot<T> {
    value: Option<T>;
    version: number;
}