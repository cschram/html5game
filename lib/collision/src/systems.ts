import { Option, Some, None, match } from "oxide.ts";
import { Query, EntityStore, Resources } from "@rez2d/ecs";
import { Transform } from "@rez2d/common";
import { RenderResources } from "@rez2d/render";
import { Collider } from "./components";
import { collide } from "./collision";
import { Rect } from "./rect";
import { CollisionResources, Collision } from "./types";

// Maximum number of iterations the module will calculate
// movement when handling collisions. i.e., for
// CollisionResponse.REFLECT and MAX_COLLISION_ITERATIONS = 5
// an object will only "reflect" 5 times in one frame.
const MAX_COLLISION_ITERATIONS = 5;

export function PopulateTree<TResources extends CollisionResources>(
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const tree = resources.get("collisionTree").unwrap();
    tree.clear();
    for (const [id, { pos, scale, rotation }, { body }] of store.query(new Query(Transform, Collider))) {
        if (body.pos.x !== pos.x || body.pos.y !== pos.y) {
            body.pos = pos;
        }
        if (!body.scale.equals(scale)) {
            body.scale = scale;
        }
        if (body.rotation !== rotation) {
            body.rotation = rotation;
        }
        tree.insert({
            area: body.bounds,
            data: [id, body],
        });
    }
}

export function MoveColliders<TResources extends CollisionResources & RenderResources>(
    store: EntityStore<TResources>,
    resources: Resources<TResources>,
) {
    const tree = resources.get("collisionTree").unwrap();
    const dt = resources.get("dt").unwrap();
    for (const [id, translation, { body, events }] of store.query(new Query(Transform, Collider))) {
        events.empty();
        const vel = body.velocity.mul(dt);
        if (vel.length > 0) {
            let remaining = 1;
            let iterations = 0;
            while (iterations < MAX_COLLISION_ITERATIONS) {
                let collision: Option<Collision> = None;
                let movement = vel.mul(remaining);
                const area = Rect.container([body.bounds, body.bounds.translate(movement.x, movement.y)]);
                for (const [pId, pBody] of tree.query(area)) {
                    if (body != pBody && body.bounds.intersects(pBody.bounds)) {
                        match(collide(body.shape, movement, pBody.shape), {
                            Some: (c) => {
                                if (collision.isNone() || c.distance < collision.unwrap().distance) {
                                    collision = Some({
                                        ...c,
                                        collider: [id, body],
                                        blocker: [pId, pBody],
                                    });
                                }
                            },
                            None: () => {},
                        });
                    }
                }

                match(collision, {
                    Some: (collision) => {
                        movement = movement.sub(collision.translation);
                        if (body.resolve(collision)) {
                            remaining -= Math.max(0, remaining * collision.distance);
                        } else {
                            remaining = 0;
                        }
                        events.publish("collision", collision);
                    },
                    None: () => {
                        remaining = 0;
                    },
                });

                body.pos = body.pos.add(movement);
                iterations++;
            }
            translation.pos = body.pos;
        }
    }
};
