import { Vec } from "ella-math";
import { Rect } from "./rect";
import { Shape } from "./shape";
import { CollisionResolver, stopResolver } from "./resolvers";
import { CollisionInfo } from "./collision";

export class Body {
    private _pos: Vec;
    private _shape: Shape;
    private _velocity: Vec;
    private _resolver: CollisionResolver;

    constructor(pos: Vec, shape: Shape, resolver: CollisionResolver = stopResolver) {
        this._pos = pos;
        this._shape = shape;
        this._shape.center = pos;
        this._velocity = new Vec(0, 0);
        this._resolver = resolver;
    }

    get pos(): Vec {
        return this._pos;
    }

    set pos(center: Vec) {
        this._pos = center;
        this._shape.center = center;
    }

    get scale(): Vec {
        return this._shape.scale;
    }

    set scale(scale: Vec) {
        this._shape.scale = scale;
    }

    get rotation(): number {
        return this._shape.rotation;
    }

    set rotation(angle: number) {
        this._shape.rotation = angle;
    }

    get shape(): Shape {
        return this._shape;
    }

    get bounds(): Rect {
        return this._shape.bounds;
    }

    get velocity(): Vec {
        return this._velocity;
    }

    set velocity(vel: Vec) {
        this._velocity = vel;
    }

    resolve(collision: CollisionInfo): boolean {
        return this._resolver(this, collision);
    }
}
