import { Vec } from "ella-math";
import { Body } from "./body";
import { CollisionInfo } from "./collision";

export type CollisionResolver = (collider: Body, collision: CollisionInfo) => boolean;

export function stopResolver(collider: Body) {
    collider.velocity = new Vec(0, 0);
    return false;
}

export function reflectResolver(collider: Body, collision: CollisionInfo) {
    const dot = 2 * collision.normal.dot(collider.velocity);
    collider.velocity = collider.velocity.sub(collision.normal.mul(dot));
    return true;
}
