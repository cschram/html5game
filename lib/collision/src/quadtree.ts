enum Quadrant {
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
}

export interface QuadTreeArea {
    left: number;
    right: number;
    top: number;
    bottom: number;
}

export interface QuadTreeEntry<TData> {
    area: QuadTreeArea;
    data: TData;
}

export class QuadTree<TData> {
    static maxDepth = 10;
    static maxBodies = 10;

    private area: QuadTreeArea;
    private level: number;
    private entries: QuadTreeEntry<TData>[];
    private subnodes: QuadTree<TData>[];

    constructor(area: QuadTreeArea, level = 0) {
        this.area = area;
        this.level = level;
        this.entries = [];
        this.subnodes = [];
    }

    clear() {
        this.entries = [];
        this.subnodes = [];
    }

    resize(size: QuadTreeArea) {
        this.clear();
        this.area = size;
    }

    insert(entry: QuadTreeEntry<TData>): void {
        if (this.subnodes.length > 0) {
            const index = this.getIndex(entry.area);
            if (index) {
                return this.subnodes[index].insert(entry);
            }
        }

        this.entries.push(entry);

        if (this.entries.length > QuadTree.maxBodies && this.level < QuadTree.maxDepth) {
            if (this.subnodes.length > 0) {
                return;
            }
            this.split();
            const children: QuadTreeEntry<TData>[] = [];
            this.entries.forEach((child) => {
                const index = this.getIndex(child.area);
                if (index) {
                    this.subnodes[index].insert(child);
                } else {
                    children.push(child);
                }
            });
            this.entries = children;
        }
    }

    *query(area: QuadTreeArea): IterableIterator<TData> {
        for (const entry of this.entries) {
            yield entry.data;
        }
        if (this.subnodes.length > 0) {
            const index = this.getIndex(area);
            if (index) {
                for (const entry of this.subnodes[index].query(area)) {
                    yield entry;
                }
            } else {
                for (const subnode of this.subnodes) {
                    for (const entry of subnode.query(area)) {
                        yield entry;
                    }
                }
            }
        }
    }

    private split() {
        if (this.subnodes.length > 0) {
            throw new Error("Attempted to split QuadTree node with subnodes");
        }
        const w = (this.area.right - this.area.left) / 2;
        const h = (this.area.bottom - this.area.top) / 2;
        const level = this.level + 1;
        this.subnodes = [
            // Top Left
            new QuadTree(
                {
                    left: this.area.left,
                    right: this.area.left + w,
                    top: this.area.top,
                    bottom: this.area.top + h,
                },
                level,
            ),
            // Top Right
            new QuadTree(
                {
                    left: this.area.left + w,
                    right: this.area.right,
                    top: this.area.top,
                    bottom: this.area.top + h,
                },
                level,
            ),
            // Bottom Left
            new QuadTree(
                {
                    left: this.area.left,
                    right: this.area.left + w,
                    top: this.area.top + h,
                    bottom: this.area.bottom,
                },
                level,
            ),
            // Bottom Right
            new QuadTree(
                {
                    left: this.area.left + w,
                    right: this.area.right,
                    top: this.area.top + h,
                    bottom: this.area.bottom,
                },
                level,
            ),
        ];
    }

    private getIndex(area: QuadTreeArea): Quadrant | null {
        if (this.subnodes.length > 0) {
            const cx = this.area.right - this.area.left;
            const cy = this.area.bottom - this.area.top;
            if (area.bottom < cy) {
                if (area.right < cx) {
                    return Quadrant.TOP_LEFT;
                } else if (area.left > cx) {
                    return Quadrant.TOP_RIGHT;
                }
            } else if (area.top > cy) {
                if (area.right < cx) {
                    return Quadrant.BOTTOM_LEFT;
                } else if (area.left > cx) {
                    return Quadrant.BOTTOM_RIGHT;
                }
            }
        }
        return null;
    }
}
