import { Vec } from "ella-math";
import { createComponent, EntityId } from "@rez2d/ecs";
import { EventQueue } from "@rez2d/events";
import { Body } from "./body";
import { Shape } from "./shape";
import { CollisionResolver } from "./resolvers";
import { Collision } from "./types";

export type ColliderEvents = {
    "collision": Collision,
};

export interface ColliderArgs {
    shape: Shape;
    velocity?: Vec;
    resolver?: CollisionResolver;
}

export const Collider = createComponent((_resources, args: ColliderArgs) => {
    const body = new Body(new Vec(0, 0), args.shape, args.resolver);
    if (args.velocity) {
        body.velocity = args.velocity;
    }
    return {
        body,
        events: new EventQueue<ColliderEvents>(),
    };
});
