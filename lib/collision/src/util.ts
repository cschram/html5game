import { Vec } from "ella-math";

export function normal(vec: Vec): Vec {
    return new Vec(
        vec.y,
        -vec.x
    ).normalized;
}

export function clamp(value: number, min: number, max: number): number {
    return Math.max(min, Math.min(max, value));
}

export function rotate(vec: Vec, angle: number, origin?: Vec): Vec {
    if (!origin) {
        origin = new Vec(0, 0);
    }
    return new Vec(
        ((vec.x - origin.x) * Math.cos(angle)) - ((vec.y - origin.y) * Math.sin(angle)) + origin.x,
        ((vec.x - origin.x) * Math.sin(angle)) + ((vec.y - origin.y) * Math.cos(angle)) + origin.y,
    );
}
