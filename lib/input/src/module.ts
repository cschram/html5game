import { Module } from "@rez2d/ecs";
import { Input } from "./input";

export interface InputConfig {
    container: HTMLCanvasElement;
}

export interface InputResources {
    input: Input;
}

export function InputModule<TResources extends InputResources>(config: InputConfig): Module<TResources> {
    return (_store, _systems, resources) => {
        resources.set("input", new Input(config.container));
    };
}
