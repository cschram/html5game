import { Option, Some, None } from "oxide.ts";

export class Event<TEvents extends Record<string, unknown>, TEventName extends keyof TEvents> {
    constructor(
        private name: TEventName,
        private message: TEvents[TEventName],
    ) {}

    is(name: string): boolean {
        return this.name === name;
    }

    get(name: string): Option<TEvents[TEventName]> {
        if (this.is(name)) {
            return Some(this.message);
        } else {
            return None;
        }
    }
}

export class EventQueue<TEvents extends Record<string, unknown>> {
    private _queue: Event<TEvents, keyof TEvents>[];

    constructor() {
        this._queue = [];
    }

    publish<TEventName extends keyof TEvents>(name: TEventName, message: TEvents[TEventName]) {
        this._queue.push(new Event(name, message));
    }

    *[Symbol.iterator]() {
        for (const event of this._queue) {
            yield event;
        }
    }

    *drain(): IterableIterator<Event<TEvents, keyof TEvents>> {
        while (this._queue.length > 0) {
            yield this._queue.pop() as Event<TEvents, keyof TEvents>;
        }
    }

    *each<TEventName extends keyof TEvents>(name: TEventName): IterableIterator<TEvents[TEventName]> {
        for (const event of this._queue) {
            if (event.is(name as string)) {
                yield event.get(name as string).unwrap() as TEvents[TEventName];
            }
        }
    }

    empty() {
        this._queue = [];
    }
}
