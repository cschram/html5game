import { EventQueue } from "./events";

type TestEvents = {
    "one": string;
    "two": number;
}

describe("EventQueue", () => {
    let queue: EventQueue<TestEvents>;

    beforeEach(() => {
        queue = new EventQueue();
    });

    it("should publish and iterate events", () => {
        queue.publish("one", "test");
        queue.publish("two", 42);
        const iter = queue[Symbol.iterator]();
        const first = iter.next();
        expect(first.value).toBeTruthy();
        expect(first.value?.is("one")).toBe(true);
        expect(first.value?.is("two")).toBe(false);
        expect(first.value?.get("one").unwrap()).toBe("test");
        expect(first.value?.get("two").isNone()).toBe(true);
        const second = iter.next();
        expect(second.value).toBeTruthy();
        expect(second.value?.is("one")).toBe(false);
        expect(second.value?.is("two")).toBe(true);
        expect(second.value?.get("one").isNone()).toBe(true);
        expect(second.value?.get("two").unwrap()).toBe(42);
    });

    it("should iterate over a single event", () => {
        queue.publish("one", "test1");
        queue.publish("two", 42);
        queue.publish("one", "test2");
        const fn = jest.fn();
        for (const event of queue.each("one")) {
            fn(event);
        }
        expect(fn).toBeCalledTimes(2);
        expect(fn).toBeCalledWith("test1");
        expect(fn).toBeCalledWith("test2");
    });
});
