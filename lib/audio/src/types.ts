import { AudioPlayer } from "./player";

export interface AudioResources {
    audio: AudioPlayer;
}
