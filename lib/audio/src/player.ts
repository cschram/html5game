import { Vec } from "ella-math";
import { Howler } from "howler";
import { Sound, SoundOptions } from "./sound";

export class AudioPlayer {
    private _sounds: Sound[];

    constructor() {
        this._sounds = [];
    }

    get volume(): number {
        return Howler.volume();
    }

    set volume(volume: number) {
        Howler.volume(volume);
    }

    get pos(): Vec {
        const [x, y, z] = Howler.pos();
        return new Vec(x, y, z);
    }

    set pos(pos: Vec) {
        Howler.pos(pos.x, pos.y, pos.z);
    }

    play(src: string, options?: Omit<SoundOptions, "autoplay">): Sound {
        const sound = new Sound(src, {
            ...options,
            autoplay: true,
        });
        this._sounds.push(sound);
        return sound;
    }

    stop(src: string) {
        for (const sound of this._sounds) {
            if (sound.src === src) {
                sound.stop();
            }
        }
    }

    update() {
        const stopped: Sound[] = [];
        for (const sound of this._sounds) {
            for (const _ of sound.events.each("stop")) {
                stopped.push(sound);
            }
        }
        this._sounds = this._sounds.filter(
            (sound) => stopped.indexOf(sound) === -1
        );
    }

    clearEvents() {
        for (const sound of this._sounds) {
            sound.events.empty();
        }
    }
}
