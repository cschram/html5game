import { Howl } from "howler";
import { Vec } from "ella-math";
import { EventQueue } from "@rez2d/events";

export interface SoundOptions {
    volume?: number;
    loop?: boolean;
    autoplay?: boolean;
}

const events = ["load", "play", "end", "pause", "stop", "mute", "volume", "rate", "seek", "fade", "unlock"];
const errorEvents = ["loaderror", "playerror"];

type SoundEvents = {
    load: void;
    play: void;
    end: void;
    pause: void;
    stop: void;
    mute: void;
    volume: void;
    rate: void;
    seek: void;
    fade: void;
    unlock: void;
    loaderror: unknown;
    playerror: unknown;
};

export class Sound {
    private _src: string;
    private _sound: Howl;
    private _events: EventQueue<SoundEvents>;

    constructor(src: string, options?: SoundOptions) {
        this._src = src;
        this._sound = new Howl({
            src,
            volume: options?.volume,
            loop: options?.loop,
            autoplay: options?.autoplay,
        });
        this._events = new EventQueue();
        for (const eventName of events) {
            this._sound.on(eventName, () => {
                this._events.publish(eventName as keyof SoundEvents, null);
            });
        }
        for (const eventName of errorEvents) {
            this._sound.on(eventName, (_, error) => {
                this._events.publish(eventName as keyof SoundEvents, error);
            });
        }
    }

    get src(): string {
        return this._src;
    }

    get events(): EventQueue<SoundEvents> {
        return this._events;
    }

    get playing(): boolean {
        return this._sound.playing();
    }

    get volume(): number {
        return this._sound.volume();
    }

    set volume(vol: number) {
        this._sound.volume(vol);
    }

    get pos(): Vec {
        const [x, y, z] = this._sound.pos();
        return new Vec(x, y, z);
    }

    set pos(pos: Vec) {
        this._sound.pos(pos.x, pos.y, pos.z);
    }

    play() {
        this._sound.play();
    }

    pause() {
        this._sound.pause();
    }

    stop() {
        this._sound.stop();
    }

    mute() {
        this._sound.mute();
    }

    unmute() {
        this._sound.mute(false);
    }

    seek(seek: number) {
        this._sound.seek(seek);
    }

    restart() {
        this._sound.seek(0);
        this._sound.play();
    }
}
