import { BitSet } from "./bitset";

describe("BitSet", () => {
    it("should have the correct length", () => {
        const bs = new BitSet(16);
        expect(bs.length).toBe(16);
    });

    it("clone should create a clone with the same values", () => {
        const bs1 = new BitSet(2);
        bs1.set(1);
        const bs2 = bs1.clone();
        expect(bs2.get(1).unwrap()).toBe(true);
    });

    it("should set bits", () => {
        const bs = new BitSet(1);
        expect(bs.get(0).unwrap()).toBe(false);
        expect(bs.set(0)).toBe(true);
        expect(bs.get(0).unwrap()).toBe(true);
    });

    it("should not set bits when out of range", () => {
        const bs = new BitSet(1);
        expect(bs.set(64)).toBe(false);
    });

    it("should not get bit when out of range", () => {
        const bs = new BitSet(1);
        expect(bs.get(64).isNone()).toBe(true);
    });

    it("should set bits from another bitset", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(4);
        bs1.set(0);
        bs2.set(0);
        bs2.set(2);
        expect(bs1.setFrom(bs2)).toBe(true);
        expect(bs1.get(0).unwrap()).toBe(true);
        expect(bs1.get(2).unwrap()).toBe(true);
    });

    it("should not set bits from another bitset if the lengths don't match", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(64);
        expect(bs1.setFrom(bs2)).toBe(false);
    });

    it("should unset bits", () => {
        const bs = new BitSet(1);
        bs.set(0);
        expect(bs.unset(0)).toBe(true);
        expect(bs.get(0).unwrap()).toBe(false);
    });

    it("should not unset bits when out of range", () => {
        const bs = new BitSet(1);
        expect(bs.unset(64)).toBe(false);
    });

    it("should unset bits from another bitset", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(4);
        bs1.set(0);
        bs1.set(1);
        bs2.set(0);
        expect(bs1.unsetFrom(bs2)).toBe(true);
        expect(bs1.get(0).unwrap()).toBe(false);
        expect(bs1.get(1).unwrap()).toBe(true);
    });

    it("should not set bits from another bitset if the lengths don't match", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(64);
        expect(bs1.unsetFrom(bs2)).toBe(false);
    });

    it("should toggle bits", () => {
        const bs = new BitSet(1);
        expect(bs.toggle(0)).toBe(true);
        expect(bs.get(0).unwrap()).toBe(true);
        expect(bs.toggle(0)).toBe(true);
        expect(bs.get(0).unwrap()).toBe(false);
    });

    it("should not toggle bits when out of range", () => {
        const bs = new BitSet(1);
        expect(bs.toggle(64)).toBe(false);
    });

    it("should clear all bits", () => {
        const bs = new BitSet(2);
        bs.set(0);
        bs.clear();
        expect(bs.get(0).unwrap()).toBe(false);
        expect(bs.get(1).unwrap()).toBe(false);
    });

    it("should be equal when bitsets are the same", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(4);
        bs1.set(0);
        bs1.set(2);
        bs2.set(0);
        bs2.set(2);
        expect(bs1.isEqual(bs2)).toBe(true);
    });

    it("bitsets should not be equal when bitsets are different", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(4);
        bs1.set(0);
        bs1.set(2);
        bs2.set(1);
        bs2.set(3);
        expect(bs1.isEqual(bs2)).toBe(false);
    });

    it("should not be equal when bitsets have different length", () => {
        const bs1 = new BitSet(4);
        const bs2 = new BitSet(5);
        expect(bs1.isEqual(bs2)).toBe(false);
    });

    it("should be a subset if its bits are set in the other", () => {
        const bs1 = new BitSet(32);
        const bs2 = new BitSet(64);
        bs1.set(0);
        bs2.set(0);
        bs2.set(32);
        expect(bs1.isSubsetOf(bs2)).toBe(true);
    });

    it("should not be a subset if its bits are not set in the other", () => {
        const bs1 = new BitSet(32);
        const bs2 = new BitSet(64);
        bs1.set(1);
        bs2.set(0);
        bs2.set(32);
        expect(bs1.isSubsetOf(bs2)).toBe(false);
    });

    it("should intersect if it has set bits shared", () => {
        const bs1 = new BitSet(32);
        const bs2 = new BitSet(64);
        bs1.set(0);
        bs1.set(1);
        bs2.set(0);
        bs2.set(32);
        expect(bs1.intersects(bs2)).toBe(true);
    });

    it("should not intersect if it does not have set bits shared", () => {
        const bs1 = new BitSet(32);
        const bs2 = new BitSet(64);
        bs1.set(1);
        bs1.set(2);
        bs2.set(0);
        bs2.set(32);
        expect(bs1.intersects(bs2)).toBe(false);
    });
});