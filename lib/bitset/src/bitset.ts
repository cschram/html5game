/**
 * Based off fast-bitset <https://github.com/mattkrick/fast-bitset>
 * Extracted only the methods that are relevant for this use case.
 */

import { Option, Some, None, match } from "oxide.ts";

const BITS_PER_INT = 31;

export class BitSet {
    private array: Uint32Array;
    private maxBit: number;

    constructor(size: number) {
        const wordCount = Math.ceil(size / BITS_PER_INT);
        this.array = new Uint32Array(wordCount);
        this.maxBit = size - 1;
    }

    get length(): number {
        return this.maxBit + 1;
    }

    clone(): BitSet {
        const cloned = new BitSet(this.length);
        for (let i = 0; i < this.array.length; i++) {
            cloned.array[i] = this.array[i];
        }
        return cloned;
    }

    get(idx: number): Option<boolean> {
        return this.getWord(idx)
            .map((word) => ((this.array[word] >> idx % BITS_PER_INT) & 1) === 1);
    }

    set(idx: number): boolean {
        return match(this.getWord(idx), {
            Some: (word) => {
                this.array[word] |= 1 << idx % BITS_PER_INT;
                return true;
            },
            None: () => false,
        });
    }

    setFrom(other: BitSet): boolean {
        if (other.array.length > this.array.length) {
            return false;
        }
        for (let i = 0; i < other.array.length; i++) {
            this.array[i] |= other.array[i];
        }
        return true;
    }

    unset(idx: number): boolean {
        return match(this.getWord(idx), {
            Some: (word) => {
                this.array[word] &= ~(1 << idx % BITS_PER_INT);
                return true;
            },
            None: () => false,
        });
    }

    unsetFrom(other: BitSet): boolean {
        if (other.array.length > this.array.length) {
            return false;
        }
        for (let i = 0; i < other.array.length; i++) {
            this.array[i] &= ~other.array[i];
        }
        return true;
    }

    toggle(idx: number): boolean {
        return match(this.getWord(idx), {
            Some: (word) => {
                this.array[word] ^= 1 << idx % BITS_PER_INT;
                return true;
            },
            None: () => false,
        });
    }

    clear() {
        for (let i = 0; i < this.array.length; i++) {
            this.array[i] = 0;
        }
    }

    isEqual(other: BitSet): boolean {
        if (this.length !== other.length) {
            return false;
        }
        for (let i = 0; i < this.array.length; i++) {
            if (this.array[i] !== other.array[i]) {
                return false;
            }
        }
        return true;
    }

    isSubsetOf(bs: BitSet): boolean {
        for (let i = 0; i < this.array.length; i++) {
            if ((this.array[i] & bs.array[i]) !== this.array[i]) {
                return false;
            }
        }
        return true;
    }

    intersects(bs: BitSet): boolean {
        for (let i = 0; i < this.array.length; i++) {
            if ((this.array[i] & bs.array[i]) !== 0) {
                return true;
            }
        }
        return false;
    }

    getWord(idx: number): Option<number> {
        return idx < 0 || idx > this.maxBit ? None : Some(~~(idx / BITS_PER_INT));
    }

    *[Symbol.iterator]() {
        for (let i = 0; i < this.length; i++) {
            if (this.get(i)) {
                yield i;
            }
        }
    }
}
