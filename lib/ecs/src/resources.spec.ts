import { Resources } from "./resources";

interface TestResources {
    foo: string;
    bar: number;
}

describe("Resources", () => {
    let resources: Resources<TestResources>;

    beforeEach(() => {
        resources = new Resources();
    });

    it("should get and set resources", () => {
        resources.set("foo", "test");
        resources.set("bar", 42);
        expect(resources.has("foo")).toBe(true);
        expect(resources.has("bar")).toBe(true);
        expect(resources.get("foo").unwrap()).toBe("test");
        expect(resources.get("bar").unwrap()).toBe(42);
    });

    it("should remove resources", () => {
        resources.set("foo", "test");
        resources.remove("foo");
        expect(resources.has("foo")).toBe(false);
        expect(resources.get("foo").isNone()).toBe(true);
    });

    it("should clear resources", () => {
        resources.set("foo", "test");
        resources.set("bar", 42);
        resources.clear();
        expect(resources.has("foo")).toBe(false);
        expect(resources.has("bar")).toBe(false);
        expect(resources.get("foo").isNone()).toBe(true);
        expect(resources.get("bar").isNone()).toBe(true);
    });
});