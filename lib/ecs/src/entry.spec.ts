import { BitSet } from "@rez2d/bitset";
import { EntityEntry } from "./entry";
import { Component, ComponentFactory, ComponentId, createComponent, IDGenerator } from "./component";
import { Resources } from "./resources";
import { QUERY_MASK_SIZE } from "./query";

describe("EntityEntry", () => {
    let resources: Resources;
    let ComponentOne: ComponentFactory<{ one: boolean }>;
    let ComponentTwo: ComponentFactory<{ two: boolean }>;
    let ComponentThree: ComponentFactory<{ three: boolean }>;

    beforeEach(() => {
        IDGenerator.reset();
        resources = new Resources();
        ComponentOne = createComponent(() => ({ one: true }));
        ComponentTwo = createComponent(() => ({ two: true }));
        ComponentThree = createComponent(() => ({ three: true }));
    });

    it("should get entity components", () => {
        const mask = new BitSet(QUERY_MASK_SIZE);
        const components = new Map<ComponentId, Component<unknown>>();
        mask.set(ComponentOne.id);
        components.set(ComponentOne.id, ComponentOne.create()(resources));
        mask.set(ComponentTwo.id);
        components.set(ComponentTwo.id, ComponentTwo.create()(resources));
        const entry = new EntityEntry({ index: 0, version: 1 }, mask, components);
        expect(entry.has(ComponentOne)).toBe(true);
        expect(entry.has(ComponentTwo)).toBe(true);
        expect(entry.has(ComponentThree)).toBe(false);
        const componentOne = entry.get(ComponentOne).unwrap();
        expect(componentOne.__id).toBe(0);
        expect(componentOne.one).toBe(true);
        const componentTwo = entry.get(ComponentTwo).unwrap();
        expect(componentTwo.__id).toBe(1);
        expect(componentTwo.two).toBe(true);
        expect(entry.get(ComponentThree).isNone()).toBe(true);
    });
});
