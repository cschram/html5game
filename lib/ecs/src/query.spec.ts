import { BitSet } from "@rez2d/bitset";
import { createComponent } from "./component";
import { Query, QUERY_MASK_SIZE } from "./query";

describe("Query", () => {
    const ComponentOne = createComponent(() => ({ one: true }));
    const ComponentTwo = createComponent(() => ({ two: true }));
    const ComponentThree = createComponent(() => ({ three: true }));

    it("should match correct entities", () => {
        const query = new Query(ComponentOne, ComponentTwo).exclude(ComponentThree);
        const entityOne = new BitSet(QUERY_MASK_SIZE);
        entityOne.set(ComponentOne.id);
        const entityTwo = new BitSet(QUERY_MASK_SIZE);
        entityTwo.set(ComponentOne.id);
        entityTwo.set(ComponentTwo.id);
        const entityThree = new BitSet(QUERY_MASK_SIZE);
        entityThree.set(ComponentOne.id);
        entityThree.set(ComponentTwo.id);
        entityThree.set(ComponentThree.id);
        expect(query.matches(entityOne)).toBe(false);
        expect(query.matches(entityTwo)).toBe(true);
        expect(query.matches(entityThree)).toBe(false);
    });
});
