import { Resources } from "./resources";
import { EntityStore } from "./store";
import {
    System,
    SystemSchedule,
} from "./system";

export type Module<TResources> = (
    store: EntityStore<TResources>,
    schedule: SystemSchedule<TResources>,
    resources: Resources<TResources>,
) => void;

export class ECS<TResources> {
    private store: EntityStore<TResources>;
    private schedule: SystemSchedule<TResources>;
    private resources: Resources<TResources>;
    private modules: Module<any>[];

    constructor() {
        this.store = new EntityStore();
        this.schedule = new SystemSchedule();
        this.resources = new Resources();
        this.modules = [];
    }

    addSystem(system: System<TResources>): ECS<TResources> {
        this.schedule.add(system);
        return this;
    }

    addResource<K extends keyof TResources>(name: K, value: TResources[K]): ECS<TResources> {
        this.resources.set(name, value);
        return this;
    }

    addModule(modules: Module<TResources> | Module<TResources>[]): ECS<TResources> {
        if (Array.isArray(modules)) {
            modules.forEach((mod) => this.modules.push(mod));
        } else {
            this.modules.push(modules);
        }
        return this;
    }

    start() {
        this.modules.forEach((module) => {
            module(this.store, this.schedule, this.resources);
        });
    }

    runSchedule() {
        this.schedule.run(this.store, this.resources);
    }
}
