import { Option, Some, None } from "oxide.ts";
import { BitSet } from "@rez2d/bitset";
import { EntityId } from "./store";
import { Component, ComponentId, ComponentFactory, InferComponent } from "./component";

export class EntityEntry {
    constructor(
        public readonly id: EntityId,
        private mask: BitSet,
        private components: Map<ComponentId, Component<unknown>>,
    ) {}

    has<TComponent extends ComponentFactory<unknown>>(component: TComponent): boolean {
        return this.mask.get(component.id).unwrap();
    }

    get<TComponent extends ComponentFactory<unknown>>(component: TComponent): Option<InferComponent<TComponent>> {
        if (this.has(component)) {
            return Some(this.components.get(component.id) as InferComponent<TComponent>);
        } else {
            return None;
        }
    }
}
