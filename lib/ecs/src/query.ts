import { BitSet } from "@rez2d/bitset";
import { ComponentFactory, InferComponent } from "./component";
import { EntityId } from "./store";

export const QUERY_MASK_SIZE = 64;

export type InferComponentTuple<TComponents extends [...unknown[]]> =
    TComponents extends [infer THead, ...infer TTail]
        ? [InferComponent<THead>, ...InferComponentTuple<TTail>]
        : never[];

export type QueryResult<TResources, TComponents extends [...ComponentFactory<unknown, unknown, TResources>[]]> = [
    EntityId,
    ...TComponents
];

export type QueryResultIterator<TResources, TComponents extends [...ComponentFactory<unknown, unknown, TResources>[]]> =
    IterableIterator<QueryResult<TResources, InferComponentTuple<TComponents>>>;

export class Query<TResources, TComponents extends [...ComponentFactory<unknown, unknown, TResources>[]]> {
    components: TComponents;
    mask: BitSet;
    excludeMask: BitSet;

    constructor(...components: TComponents) {
        this.components = components;
        this.mask = new BitSet(QUERY_MASK_SIZE);
        this.excludeMask = new BitSet(QUERY_MASK_SIZE);
        this.components.forEach((component) => this.mask.set(component.id));
    }

    exclude<TExcludeComponents extends [...ComponentFactory<unknown, unknown>[]]>(
        ...components: TExcludeComponents
    ): Query<TResources, TComponents> {
        components.forEach((component) => this.excludeMask.set(component.id));
        return this;
    }

    matches(entityMask: BitSet): boolean {
        return this.mask.isSubsetOf(entityMask) && !this.excludeMask.intersects(entityMask);
    }
}
